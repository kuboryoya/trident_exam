<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>upload</title>
	<style>
		table td{
			border: 1px solid #333;
		}
		img {
			max-width: 100px;
		}
	</style>
<body>
		<h1><?= $creatures['name']; ?></h1>
		<p><?= $creatures['note']; ?></p>
		<image src="<?=  Uri::base() .  $user_creatures['image'];?>">
		<a href="<?= Uri::base() ?>welcome/list">戻る</a>
		<div id="single-map" style="position: relative;overflow: hidden;height:  500px;"></div>
		<dl>
				<dt>lat</dt><dd><?= $user_creatures['lat']; ?></dd>
				<dt>lng</dt><dd><?= $user_creatures['lng']; ?></dd>
		</dl>

<script async defer
		src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCUTtnZ8_BkU3tCdJgbRVqij3R1ZS03Fbs&callback=initMap">
</script>
<script
  src="https://code.jquery.com/jquery-2.2.4.min.js"
  integrity="sha256-BbhdlvQf/xTY9gja0Dq3HiwQF8LaCRTXxZKRutelT44="
  crossorigin="anonymous">
</script>

<?= json_encode($other_creatures) ?>

<script>
	function initMap(){
		var marker = [];
		var mapData = <?php echo json_encode($other_creatures) ?>;
		console.log(mapData);
		// 地図の作成
    var mapLatLng = new google.maps.LatLng({lat: 1, lng: 1}); // 緯度経度のデータ作成
    map = new google.maps.Map(document.getElementById('single-map'), { // #single-mapに地図を埋め込む
      center: mapLatLng, // 地図の中心を指定
      zoom: 4 // 地図のズームを指定
    });
		for (var i = 0; i < mapData.length; i++) {
			console.log(mapData[i]['image']);
      markerLatLng = new google.maps.LatLng({lat: Number(mapData[i]['lat']), lng: Number(mapData[i]['lng'])}); // 緯度経度のデータ作成
      marker[i] = new google.maps.Marker({ // マーカーの追加
        position: markerLatLng, // マーカーを立てる位置を指定
        map: map // マーカーを立てる地図を指定
      });

      // infoWindow[i] = new google.maps.InfoWindow({ // 吹き出しの追加
      //   content: '<div class="single-map">' + markerData[i]['image'] + '</div>' // 吹き出しに表示する内容
      // });

      // markerEvent(i); // マーカーにクリックイベントを追加
    }

    for (var i = 0; i < mapData.length; i++) {
      marker[i].setOptions({// マーカーのオプション設定
        icon:  '<?= Uri::base() ?>' + mapData[i]['image']
      });
    }

	}
	
</script>
</body>
</html>


