<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>いきものリスト</title>
	<style>
		table td{
			border: 1px solid #333;
		}
		img {
			max-width: 100px;
		}
	</style>
<body>
	user: <?= $user ?>
	<table>
		<tr>
			<th>Name</th>
			<th>Note</th>
			<th>Image</th>
			<th>Link</th>
		</tr>
		<?php foreach ($creatures as $creature) : ?>
			<tr>
				<td><?= $creature['name']; ?></td>
				<td><?= $creature['note']; ?></td>
				<td>
					<img src="
						<?php foreach ($user_creatures as $user_creature) : ?>
							<?php if($user_creature['creature_id'] == $creature['id']) : ?>
								<?= Uri::base() . $user_creature['image']; ?>
							<?php endif ?>
						<?php endforeach ?>				
					">
				</td>
				<td><?= Html::anchor('welcome/view/'.$creature['id'],'詳細') ?></td>
			</tr>
    <?php endforeach ?>
  </table>
	<a href="<?= Uri::base() ?>welcome/upload">図鑑登録</a>
</body>
</html>


