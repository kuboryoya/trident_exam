<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>upload</title>
<body>
  <h1>図鑑登録</h1>
  <form action="" method="post" enctype="multipart/form-data">
    creature : 
    <select name="creature_id">
      <?php foreach ($creatures as $creature) : ?>
        <option value="<?= $creature['id'] ?>"><?= $creature['name'] ?></option>
      <?php endforeach ?>
    </select>
    <br>
    <input type="file" name="image"><br>
    lat : <input type="text" name="lat"><br>
    lng : <input type="text" name="lng"><br>
    <input type="submit" value="送信">
  </form>
  <a href="<?= Uri::base() ?>welcome/list">戻る</a>
</body>
</html>
