<?php

class Model_Creature extends \Orm\Model
{
	protected static $_properties = array(		
		'id',
		'name',
		'note',
	);

	protected static $_table_name = 'creature';

}
