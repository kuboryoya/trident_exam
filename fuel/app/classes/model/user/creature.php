<?php

class Model_User_Creature extends \Orm\Model
{
	protected static $_properties = array(		
		'id',
		'user_id',
		'creature_id',
		'image',
		'lat',
		'lng',
	);

	protected static $_table_name = 'user_creature';

}
