<?php
/**
 * Fuel is a fast, lightweight, community driven PHP5 framework.
 *
 * @package    Fuel
 * @version    1.8
 * @author     Fuel Development Team
 * @license    MIT License
 * @copyright  2010 - 2016 Fuel Development Team
 * @link       http://fuelphp.com
 */

/**
 * The Welcome Controller.
 *
 * A basic controller example.  Has examples of how to set the
 * response body and status.
 *
 * @package  app
 * @extends  Controller
 */
class Controller_Welcome extends Controller
{

	public function get_signup() {
		return Response::forge(View::forge('welcome/signup'));
	}
	public function post_signup() {
		$user = Model_User::forge();
		$user -> name = Input::post('name');
		$user -> password = Auth::instance()->hash_password(Input::post('pass'));
		$user -> mail = Input::post('mail');
		$user -> save();
		Response::redirect('welcome/signin');
	}

	public function get_signin() {
		return Response::forge(View::forge('welcome/signin'));
	}
	public function post_signin() {
		$users = Model_User::find('all');
		// nameとpassを照合
		foreach ($users as $user) {
			if(
				Input::post('name') == $user['name'] &&
				Auth::instance()->hash_password(Input::post('pass')) == $user['password']
			){
				Session::set('userid', $user['id']);
				Response::redirect('welcome/list');
			};
		}
		Response::redirect('welcome/signin');
	}

	public function get_upload() {
		$data = [];
		$data['creatures'] = Model_Creature::find('all');
		return Response::forge(View::forge('welcome/upload', $data));
	}
	public function post_upload() {
		$image = Input::file('image');
		if($image['name'] != ""){
			move_uploaded_file($image['tmp_name'],'assets/img/'.$image['name']);
			$model = Model_User_Creature::forge();
			$model -> user_id = Input::post('user_id', Session::get('userid'));
			$model -> creature_id = Input::post('creature_id', 0);
			$model -> image = 'assets/img/'.$image['name'];
			$model -> lat = Input::post('lat', 0);
			$model -> lng = Input::post('lng', 0);
			$model -> save();
			Response::redirect('welcome/list');
		}else{
			Response::redirect('welcome/upload');
		}
	}

	public function action_list() {
		$data = [];
		$data['user'] = Session::get('userid');
		$data['creatures'] = Model_Creature::find('all');
		$data['user_creatures'] = Model_User_Creature::find('all',
			array('where' =>
				array(
					array('user_id', $data['user'])
				)
			)
		);
		return Response::forge(View::forge('welcome/list', $data));
	}

	public function action_view($id = null)
	{
		// viewページでidを指定しない場合はリダイレクト
		is_null($id) and Response::redirect('welcome/list');
		$data['user'] = Session::get('userid');
		$data['creatures'] = Model_Creature::find('first',
			array('where' =>
				array(
					array('id', $id),
				)
			)
		);
		$data['user_creatures'] = Model_User_Creature::find('first',
			array('where' =>
				array(
					array('user_id', $data['user']),
					array('creature_id', $id),
				)
			)
		);
		$data['other_creatures'] = Model_User_Creature::find('all',
			array('where' =>
				array(
					array('user_id', '!=' , $data['user']),
					array('creature_id', $id),
				)
			)
		);

		$oc = array();
		foreach ($data['other_creatures'] as $other_creature) {
			$oc[] = array(
				'id' => $other_creature['id'],
				'user_id' => $other_creature['user_id'],
				'creature_id' => $other_creature['creature_id'],
				'image' => $other_creature['image'],
				'lat' => $other_creature['lat'],
				'lng' => $other_creature['lng'],
			);
		}
		$data['other_creatures'] = $oc;
		return Response::forge(View::forge('welcome/view', $data));
	}

	/**
	 * The basic welcome message
	 *
	 * @access  public
	 * @return  Response
	 */
	public function action_index()
	{
		return Response::forge(View::forge('welcome/index'));
	}

	/**
	 * A typical "Hello, Bob!" type example.  This uses a Presenter to
	 * show how to use them.
	 *
	 * @access  public
	 * @return  Response
	 */
	public function action_hello()
	{
		return Response::forge(Presenter::forge('welcome/hello'));
	}

	/**
	 * The 404 action for the application.
	 *
	 * @access  public
	 * @return  Response
	 */
	public function action_404()
	{
		return Response::forge(Presenter::forge('welcome/404'), 404);
	}
}
